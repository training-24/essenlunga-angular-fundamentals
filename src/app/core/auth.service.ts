import { HttpClient, HttpParams } from '@angular/common/http';
import { computed, inject, Injectable, signal } from '@angular/core';
import { AsyncSubject, BehaviorSubject, delay } from 'rxjs';

interface Auth {
  token: string;
  role: string;
  displayName: string;
}

@Injectable()
export class AuthService {
  auth = signal<Auth | null>(null)
  authNormal: Auth | null = null;
  http = inject(HttpClient)
  // auth$ = new AsyncSubject<Auth | null>()
  isLogged = computed(() => !!this.auth())
  role = computed(() => this.auth()?.role)

  // displayName = computed(() => this.)

  constructor() {
    console.log('auth service constructuro')
  }

  login(u: string) {
    const params = new HttpParams()
      .set('usernane', u)
      .set('pass', 123)

    this.http.get<Auth>('http://localhost:3000/login', { params })
      .pipe(
        delay(1000)
      )
      .subscribe(res => {
        this.authNormal = res;
        this.auth.set(res)
      })
  }

  logout() {
    this.auth.set(null)
  }
}
