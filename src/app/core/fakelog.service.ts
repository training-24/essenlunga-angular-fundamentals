import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FakelogService {

  constructor() {
    console.log('FAKE LOG log service')
  }
}
