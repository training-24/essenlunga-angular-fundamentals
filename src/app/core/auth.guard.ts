import { HttpClient } from '@angular/common/http';
import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { delay, map } from 'rxjs';
import { AuthService } from './auth.service';

export const authGuard: CanActivateFn = (route, state) => {
  // const auth = inject(AuthService)
  const router = inject(Router)
  const http = inject(HttpClient)
  const authService = inject(AuthService)
  if (!authService.isLogged()) {
    router.navigateByUrl('login')
  }
  return authService.isLogged();


  /*return http.get<{ response: string}>('http://localhost:3000/validate')
    .pipe(
      delay(1000),
      map(res => res.response === 'ok')
    )*/
};
