import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { mySuperValue } from '../app.config';
import { LogService } from './log.service';

@Injectable({
  providedIn: 'root'
})
export class FakeUserService {

  constructor(
    private http: HttpClient,
    private logSrv: LogService,
    @Inject(mySuperValue) superValue: string
  ) {
    console.log('fake user service', superValue)
  }

  init(value: number) {

  }
}
