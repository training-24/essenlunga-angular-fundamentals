import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { mySuperValue } from '../app.config';
import { LogService } from './log.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private logSrv: LogService,
    @Inject(mySuperValue) superValue: string
  ) {
    console.log('user service', superValue)
  }
}
