import { ChangeDetectionStrategy, Component, effect, inject } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { TodosService } from '../features/todo-list/services/todos.service';
import { IfLoggedDirective } from '../shared/directives/if-logged.directive';
import { IfRoleIsDirective } from '../shared/directives/if-role-is.directive';
import { MyRouterLinkActiveDirective } from '../shared/directives/my-router-link-active.directive';
import { WidgetComponent } from '../shared/widget/widget.component';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-nav-bar',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    RouterLink,
    RouterLinkActive,
    WidgetComponent,
    MyRouterLinkActiveDirective,
    IfLoggedDirective,
    IfRoleIsDirective
  ],
  template: `
    <div class="flex gap-3 m-3 flex-wrap">
      <button class="btn btn-sm" routerLink="login" routerLinkActive="btn-info">login</button>
      <button class="btn btn-sm" routerLink="demo-forms" routerLinkActive="btn-info">Demo Forms ⬇️</button>
      <button class="btn btn-sm" routerLink="uikit" routerLinkActive="btn-info">UIKit ⬇️</button>

      <button class="btn btn-sm" routerLink="demo-pipes" routerLinkActive="btn-info">pipes</button>
      <button class="btn btn-sm" routerLink="demo-styles" routerLinkActive="btn-info">Styles</button>
      <button class="btn btn-sm" routerLink="demo-inputs" routerLinkActive="btn-info">Inputs</button>
      <button class="btn btn-sm" routerLink="demo-widgets" routerLinkActive="btn-info">Demo Widgets</button>
      
      
      <button class="btn btn-sm" routerLink="crud" routerLinkActive="btn-info">Crud</button>
      <button class="btn btn-sm" routerLink="signal1" routerLinkActive="btn-info">Signal1</button>
      <button class="btn btn-sm" routerLink="signal2" routerLinkActive="btn-info">Signal2</button>
      <button class="btn btn-sm" routerLink="signal3" routerLinkActive="btn-info">Signal3</button>
      <button class="btn btn-sm" routerLink="users" routerLinkActive="btn-info" route>users</button>
      
      <button class="btn btn-sm" *appIfLogged (click)="authService.logout()">Logout</button>
      
      <button *appIfRoleIs="'admin'">ADMIN CMS</button>
      {{ authService.auth()?.displayName }} 
    </div>
    <hr>
    
  `,
  styles: ``
})
export class NavBarComponent {
  authService = inject(AuthService)

  constructor() {
    effect(() => {
      console.log(this.authService.auth())
    });
  }
}
