import { log } from '@angular-devkit/build-angular/src/builders/ssr-dev-server';
import { HttpClient, provideHttpClient } from '@angular/common/http';
import { ApplicationConfig, inject, InjectionToken } from '@angular/core';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { environment } from '../environments/environment';

import { routes } from './app.routes';
import { AuthService } from './core/auth.service';
import { FakeUserService } from './core/fake-user.service';
import { LogService } from './core/log.service';
import { UserService } from './core/user.service';

export const mySuperValue = new InjectionToken('my-super-value')


export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes, withComponentInputBinding()),
    provideHttpClient(),
    AuthService,
    {
      provide: UserService,
      useFactory: (http: HttpClient, logSrv: LogService, mySuperValue: string) => {
        // ...
        return environment.production ?
          new UserService(http, logSrv, mySuperValue) :
          new FakeUserService(http, logSrv, mySuperValue)
      },
      deps: [HttpClient, LogService, 'mySuperValue' ]
    },
    {
      provide: 'mySuperValue',
      useValue: 'pippo'
    }
  ]
};

