import { JsonPipe, UpperCasePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, effect, inject, input, Input, signal } from '@angular/core';
import { delay } from 'rxjs';
import { Meteo } from '../model/meteo';

@Component({
  selector: 'app-weather-signal',
  standalone: true,
  imports: [
    UpperCasePipe,
    JsonPipe
  ],
  template: `
    <p>
      CITY: {{cityLabel()}}
    </p>
  
    <div class="bg-gray-600 p-3 rounded-xl h-16">
      @if(meteo(); as meteo) {
        {{ meteo.main.temp }}°
      } @else {
        <div class="">loading....</div>
      }
    </div>  
  `,
  styles: ``
})
export class WeatherSignalComponent {
  meteo = signal<Meteo | null>( null );
  http = inject(HttpClient)
  city = input.required<string>()
  cityLabel = computed(() => this.city().toUpperCase())

  constructor() {
    effect(() => {
      this.meteo.set(null)
      this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city()}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => {
          this.meteo.set(res);
        })
    }, { allowSignalWrites: true });
  }
}
