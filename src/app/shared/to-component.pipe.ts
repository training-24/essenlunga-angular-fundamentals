import { Pipe, PipeTransform, Type } from '@angular/core';
import { CardComponent } from './card.component';
import { WeatherSignalComponent } from './weather-signal.component';


export const COMPONENTS: { [key: string]: Type<any> } = {
  'weather': WeatherSignalComponent,
  'card': CardComponent
}

@Pipe({
  name: 'toComponent',
  standalone: true
})
export class ToComponentPipe implements PipeTransform {

  transform(key: string, ): Type<any> {
    return COMPONENTS[key];
  }

}
