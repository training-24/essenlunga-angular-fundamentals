import { Component, inject, input, model, output, signal } from '@angular/core';
import { AccordionComponent } from './accordion.component';
import { DisaccordionComponent } from './disaccordion.component';

@Component({
  selector: 'app-disaccordion-item',
  standalone: true,
  imports: [],
  template: `
    <div>
      <div 
        class="text-xl bg-black text-white p-3"
        (click)="toggle.emit()"
      >{{title()}}</div>
      @if(isOpen()) {
        <div>
          <ng-content></ng-content>
        </div>  
      }
      
    </div>
  `,
  styles: ``
})
export class DisaccordionItemComponent {
  title = input()
  isOpen = model(false)
  toggle = output()

  accordion = inject(DisaccordionComponent, { optional: true })

  constructor() {
    if (this.accordion) {
      setTimeout(() => {
        // console.log(this.accordion?.isFirstOpen())
      }, 1000)
    }

  }
}
