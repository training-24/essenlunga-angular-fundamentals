import {
  AfterViewInit,
  Component,
  computed,
  effect,
  ElementRef,
  input, output,
  signal,
  untracked,
  viewChild
} from '@angular/core';
import { LatLngExpression } from 'leaflet';

import * as L from 'leaflet'

export type MapCoords = LatLngExpression


@Component({
  selector: 'app-leaflet',
  standalone: true,
  imports: [],
  template: `
    <div
      #mapRef class="map"
      (mouseleave)="mouseLeave.emit()"
    ></div>
  `,
  styles: `
    .map { height: 180px;  }
  `
})
export class LeafletComponent {
  mapRef = viewChild.required<ElementRef<HTMLDivElement>>('mapRef')
  coords = input.required<LatLngExpression>()
  zoom = input(10)
  map!: L.Map;
  mouseLeave = output()

  constructor() {
    effect(() => {
      this.map = L.map(this.mapRef().nativeElement)
        .setView(untracked(this.coords), untracked(this.zoom));

      L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }).addTo(this.map);
    });

    effect(() => {
      this.map.setView(this.coords())
    });

    effect(() => {
      this.map.setZoom(this.zoom())
    });
  }
}
