import { NgClass } from '@angular/common';
import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-rating',
  standalone: true,
  imports: [
    NgClass
  ],
  template: `

    @for (star of [null,null,null,null,null]; track star) {
      <i 
        class="fa"
        (click)="onClickStar($index + 1)"
        [ngClass]="{
          'fa-star': $index < value,
          'fa-star-o':  $index >= value
        }"
      ></i>
    }
  `,
  styles: ``,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RatingComponent),
      multi: true
    }
  ]
})
export class RatingComponent implements ControlValueAccessor {
  onChange!: (rate: number) => void;
  onTouched!: () => void;
  value: number = 0;

  writeValue(rate: number): void {
    this.value = rate;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  onClickStar(newRate: number) {
    this.value = newRate;
    this.onChange(newRate)
  }

}
