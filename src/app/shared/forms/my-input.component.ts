import { Component, forwardRef, input, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-my-input',
  standalone: true,
  imports: [],
  template: `
    <div class="">
      <div>{{ label() }}</div>
      <input
        type="text" 
        class="input input-bordered"
        [value]="value"
        (input)="changeModel(inputRef.value)"
        #inputRef
        (blur)="onTouched()"
      >
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MyInputComponent),
      multi: true
    }
  ]
})
export class MyInputComponent implements ControlValueAccessor{
  label = input('')
  value: string = '';

  onChange!: (value: string) => void;
  onTouched!: () => void;

  writeValue(text: string): void {
    this.value = text;
  }

  changeModel(text: string) {
    this.onChange(text);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

}
