import { NgClass } from '@angular/common';
import { Component, forwardRef, input, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-color-picker',
  standalone: true,
  imports: [
    NgClass
  ],
  template: `
    
    {{activeColor}}
    <div class="flex gap-2 p-3 border border-white rounded-xl">
      @for(color of colors(); track color) {
        <div 
          class="w-4 h-4"
          [ngClass]="{
            'border-2 border-white': color === activeColor,
            'opacity-40': isDisabled
          }"
          [style.background-color]="color"
          (click)="changeColor(color)"
        ></div>
      }
    </div>
  `,
  styles: ``,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ColorPickerComponent),
      multi: true
    }
  ]
})
export class ColorPickerComponent implements ControlValueAccessor  {
  colors = input(['#F44336', '#90CAF9', '#FFCDD2', '#69F0AE', '#AED581', '#FFE082'])
  activeColor: string = '';
  isDisabled: boolean = false;
  onChange!: (color: string) => void;
  onTouched!: () => void;

  writeValue(color: string): void {
    this.activeColor = color;
  }

  changeColor(color: string) {
    this.activeColor = color;
    this.onTouched();
    this.onChange(color)
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
