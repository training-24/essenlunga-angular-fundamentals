import { Component, forwardRef, inject, Input } from '@angular/core';
import {
  ControlValueAccessor, FormBuilder,
  FormControl,
  FormGroup,
  FormsModule, NG_VALIDATORS, NG_VALUE_ACCESSOR,
  ReactiveFormsModule,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-my-car-form',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule
  ],
  template: `

    <div [formGroup]="form">
        <input class="input input-bordered" type="text" formControlName="brand">
        <input class="input input-bordered" type="text" formControlName="model">
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MyCarFormComponent),
      multi: true
    },
    // {
    //   provide: NG_VALIDATORS,
    //   useExisting: forwardRef(() => MyInput2Component),
    //   multi: true
    // }
  ]
})
export class MyCarFormComponent implements ControlValueAccessor {
  form = inject(FormBuilder).group({
    brand: ['', Validators.required],
    model: ''
  })

  writeValue(obj: any): void {
    this.form.setValue(obj)
  }

  registerOnChange(fn: any): void {
    this.form.valueChanges.subscribe(formData => fn(formData))
  }

  registerOnTouched(fn: any): void {
      // throw new Error('Method not implemented.');
  }


}
