import { JsonPipe, NgClass } from '@angular/common';
import {
  booleanAttribute,
  Component,
  forwardRef,
  inject,
  Injector,
  input,
  Input,
  OnDestroy,
  OnInit
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl, NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  NgControl,
  ReactiveFormsModule,
  ValidationErrors,
  Validator
} from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-my-input2',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    NgClass,
    JsonPipe
  ],
  template: `
    <div class="">
      <em>{{ label() }}</em>

      <!--<pre>{{ngControl.errors|json}}</pre>-->
      <div>
      @if (ngControl.errors?.['required']) {
        <div>Required</div>
      }
      @if (ngControl.errors?.['minlength']) {
        <div>too short</div>
      }
      @if (ngControl.errors?.['alphaNumeric']) {
        <div>must be alphanumeric</div>
      }
      </div>
      <input
        type="text"
        class="input input-bordered"
        [ngClass]="{
          'input-error': ngControl.invalid
        }"
        [formControl]="input"
        (blur)="onTouched()"
      >
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MyInput2Component),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MyInput2Component),
      multi: true
    }
  ]
})
export class MyInput2Component implements ControlValueAccessor, Validator, OnInit, OnDestroy {
  @Input({ transform: booleanAttribute })
  alphaNumeric: boolean = false;

  label = input('')
  input = new FormControl('', { nonNullable: true})
  onTouched!: () => void;
  sub!: Subscription;
  inj = inject(Injector)
  ngControl!: NgControl;

  ngOnInit() {
    this.ngControl = this.inj.get(NgControl)
  }

  writeValue(text: string): void {
    this.input.setValue(text)
  }

  registerOnChange(fn: any): void {
    this.sub = this.input.valueChanges
      .subscribe(fn)
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  validate(control: AbstractControl<any, any>): ValidationErrors | null {
    if (this.alphaNumeric && !control.value.match(ALPHA_NUMERIC_REGEX)) {
      return { alphaNumeric: true };
    }

    return null;
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}


const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;
