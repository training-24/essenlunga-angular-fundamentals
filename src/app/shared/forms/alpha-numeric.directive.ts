import { Directive, forwardRef } from '@angular/core';
import { AbstractControl, FormControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[alphaNumeric]',
  standalone: true,
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => AlphaNumericDirective),
      multi: true
    }
  ]
})
export class AlphaNumericDirective implements Validator{

  validate(c: AbstractControl): ValidationErrors | null{
    const isValid = /^[a-zA-Z]+$/.test(c.value);

    return isValid ?
      null :
      {
        alphaNumeric: {
          message: 'no symbols allowed'
        }
      }
  }

}
