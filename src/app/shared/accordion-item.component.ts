import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-accordion-item',
  standalone: true,
  imports: [],
  template: `
    <div class="collapse bg-base-200">
      <input type="radio" [name]="groupName" checked="checked" />
      <div class="collapse-title text-xl font-medium flex justify-between">
        <div>{{ title }}</div>
      
      </div>
      <div class="collapse-content">
        <div>
         <ng-content></ng-content>
        </div>

        <button
          class="btn btn-primary"
          (click)="buttonClick.emit()"
        >
          {{ buttonLabel }}</button>
      </div>
    </div>
  `,
  styles: ``
})
export class AccordionItemComponent {
  @Input({ required: true }) title: string | undefined;
  @Input() groupName: string = 'accordion-default'
  @Input() buttonLabel: string | undefined
  @Output() buttonClick = new EventEmitter()
}
