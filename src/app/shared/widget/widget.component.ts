import { Component, inject } from '@angular/core';
import { WidgetService } from './widget.service';

@Component({
  selector: 'app-widget',
  standalone: true,
  imports: [],
  template: `
    @if(widgetSrv) {
      <div (click)="widgetSrv.inc()">
        widget {{widgetSrv.counter}}
      </div>
    } @else {
      <div>Welcome!!</div>
    }
  `,
  styles: ``,
  providers: [
    WidgetService
  ]
})
export class WidgetComponent {
  widgetSrv = inject(WidgetService, { optional: true })

  ngOnInit() {
    console.log('init', this.widgetSrv)
  }


  ngOnDestroy() {
    // this.widgetSrv.destroy()
  }
}
