import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AccordionItemComponent } from './accordion-item.component';
import { TimeLine } from './timeline.component';

@Component({
  selector: 'app-accordion',
  standalone: true,
  imports: [
    AccordionItemComponent
  ],
  template: `

    @for(item of data; track $index) {
      <app-accordion-item 
        [title]="item.start"
        [buttonLabel]="buttonLabel"
        (buttonClick)="buttonClick.emit(item)"
      >
        {{item.end}}
      </app-accordion-item>
    }

  `,
  styles: ``
})
export class AccordionComponent {
  @Input() data: TimeLine[] = [];
  @Input() buttonLabel: string | undefined;
  @Output() buttonClick = new EventEmitter<TimeLine>()
}
