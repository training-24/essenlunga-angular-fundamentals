import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-title',
  standalone: true,
  imports: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <h1 class="text-5xl mb-8">
      <ng-content></ng-content>
    </h1>
  `,
  styles: ``
})
export class TitleComponent {
}
