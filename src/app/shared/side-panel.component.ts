import { NgClass } from '@angular/common';
import { Component, input, model } from '@angular/core';

@Component({
  selector: 'app-side-panel',
  standalone: true,
  imports: [
    NgClass
  ],
  template: `
 
    <div 
      class="fixed top-0 bottom-0 w-96 bg-slate-800 transition-all duration-1000"
      [ngClass]="{
        '-left-96': !opened(),
        'left-0': opened(),
      }"
    >
      
      <button class="btn" (click)="opened.set(false)">close</button>
      <h1 class="text-2xl">{{ title() }}</h1>

      <ng-content></ng-content>
    </div>  
  
  `,
  styles: `
  `
})
export class SidePanelComponent {
  title = input('')
  opened = model(false)
}
