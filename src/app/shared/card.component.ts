import { JsonPipe, NgClass, UpperCasePipe } from '@angular/common';
import {
  booleanAttribute, ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  numberAttribute,
  Output,
  SimpleChanges
} from '@angular/core';

@Component({
  selector: 'app-card',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgClass,
    UpperCasePipe,
    JsonPipe
  ],
  template: `
    <div 
      class="card w-full shadow-xl"
      [ngClass]="{
        'bg-slate-600': !variant,
        'bg-green-600': variant === 'success',
        'bg-red-600': variant === 'danger',
      }"
    >
      @if (imgUrl) {
        <figure>
          <img [src]="imgUrl"
               alt="Shoes" />
        </figure>  
      }
      
      <div class="card-body">
        @if(title) {
          <div class="flex justify-between">
            <h2 class="card-title">
              {{title}} 
            </h2>
            <div (click)="isOpenChange.emit(!isOpen)">
              X
            </div>
          </div>
        }

        @if (isOpen) {
          <p>
            <ng-content />
          </p>  
        }
        
        <div 
          class="card-actions"
          [ngClass]="{
            'justify-start': buttonAlignment === 'start',
            'justify-end': buttonAlignment === 'end',
            'justify-center': buttonAlignment === 'center',
          }"
        >
          @if(buttonLabel) {
            <button
              class="btn btn-primary"
              (click)="buttonEmit()"
            >
              {{ buttonLabel }}
            </button>
          }
        </div>
      </div>
      <hr>
      
      @if(showFooter) {
        <div>
          copyright bla bla
        </div>  
      }
    </div>
  `,
})
export class CardComponent {
  @Input({ transform: booleanAttribute })
  showFooter = false;

  @Input({ transform: numberAttribute}) cols = 1

  @Input({
    // required: true,
    transform: (value: string) => {
      return value.toLowerCase()
    }
  })
  title: string | undefined;

  @Input({ alias: 'src'}) imgUrl: string | undefined;
  @Input() variant: 'success' | 'danger' | undefined;
  @Input() buttonAlignment: 'start' | 'end' | 'center' = 'end'
  @Input() buttonLabel: string | undefined
  @Input() buttonUrl: string | undefined
  @Input() isOpen = true;
  @Output() isOpenChange = new EventEmitter();
  @Output() buttonClick = new EventEmitter();

  @Input() set enableSomething(val: boolean) {
    console.log('enableSomething', val)
  }
  /*
  @Input() set field1(val: string) {
    map.setCenter(val)
  }
  @Input() set field2(val: string) {
    map.setCenter(val)
  }
  */
  buttonEmit() {
    this.buttonClick.emit()
  }

  constructor() {
    console.log(this.title)
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChanges', changes)
    if (changes['isOpen']) {
      console.log('cambiato isOpen', changes['isOpen'])
    }
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit', this.title, this.isOpen)
  }
}
