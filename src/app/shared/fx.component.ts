import { NgClass } from '@angular/common';
import { Component, input } from '@angular/core';

@Component({
  selector: 'app-fx',
  standalone: true,
  imports: [
    NgClass
  ],
  template: `
    <div
      class="flex "
      [ngClass]="{
        'gap-3': gap() === 'sm',
        'gap-12': gap() === 'xl',
        'justify-center': justify() === 'center',
        'justify-between': justify() === 'between',
        'justify-start': justify() === 'start',
      }"
    >
      <ng-content></ng-content>
    </div>
  `,
  styles: ``
})
export class FxComponent {
  gap = input<'sm' | 'xl'>('sm')
  justify = input<'center' | 'between' | 'start'>('between')
  //Bdirection = input<'row' | 'col'>('row')
}
