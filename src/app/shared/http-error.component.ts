import { ChangeDetectionStrategy, Component, input } from '@angular/core';

@Component({
  selector: 'app-http-error',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    @if(error()) {
      <div class="alert alert-error">
        errore server
      </div>
    }
    {{render()}}
  `,
  styles: ``
})
export class HttpErrorComponent {
  error = input.required<boolean>()

  render() {
    console.log('app-http-errpr')
  }
}
