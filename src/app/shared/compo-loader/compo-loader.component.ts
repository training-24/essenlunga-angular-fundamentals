import {
  Component,
  effect,
  inject,
  input,
  signal,
  TemplateRef,
  Type,
  viewChild,
  ViewContainerRef
} from '@angular/core';
import { CardComponent } from '../card.component';
import { ToComponentPipe } from '../to-component.pipe';
import { WeatherSignalComponent } from '../weather-signal.component';
/*
export const COMPONENTS: { [key: string]: Type<any> } = {
  'weather': WeatherSignalComponent,
  'card': CardComponent
}*/
@Component({
  selector: 'app-compo-loader',
  standalone: true,
  imports: [],
  template: `
   
  `,
  styles: ``
})
export class CompoLoaderComponent {
  view = inject(ViewContainerRef);
  component = input.required<{ type: string, inputs: any }>()

  constructor() {
    effect(() => {
      // const compo = this.view.createComponent(COMPONENTS[this.component().type])

      const toComponent = new ToComponentPipe()
      const compo = this.view.createComponent(
        toComponent.transform(this.component().type)
      )

      for (let key in this.component().inputs) {
        compo.setInput(key, this.component().inputs[key])
      }

    });
  }
}
