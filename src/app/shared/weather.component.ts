import { HttpClient } from '@angular/common/http';
import { Component, inject, Input } from '@angular/core';
import { Router } from '@angular/router';
import { delay, interval, lastValueFrom } from 'rxjs';
import { Meteo } from '../model/meteo';

@Component({
  selector: 'app-weather',
  standalone: true,
  imports: [],
  template: `
    <p>
      weather works!
    </p>
    
      <div class="bg-gray-600 p-3 rounded-xl h-16">
        @if(meteo) {
          {{ meteo.main.temp }}°
        } @else {
          <div class="">loading....</div>
        }
      </div>  
  `,
  styles: ``
})
export class WeatherComponent {
  meteo: Meteo | null = null;
  http = inject(HttpClient)

  @Input({ required: true }) city: string | undefined;

  ngOnChanges() {
    this.meteo = null;
    this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .pipe(
        delay(2000)
      )
      .subscribe(res => {
        this.meteo = res;
      })
  }

  /*
    @Input({ required: true }) set city(val: string) {
      this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${val}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .pipe(
          delay(2000)
        )
        .subscribe(res => {
          this.meteo = res;
        })
    }*/


}
