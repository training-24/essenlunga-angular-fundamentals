import { Directive, HostListener, inject, input, ViewContainerRef } from '@angular/core';
import { TooltipContentComponent } from './tooltip-content.component';

@Directive({
  selector: '[appTooltip]',
  standalone: true
})
export class TooltipDirective {
  appTooltip = input.required()
  view = inject(ViewContainerRef)

  @HostListener('mouseover')
  mouseOver() {
    const compo = this.view.createComponent(TooltipContentComponent)
    compo.setInput('title', this.appTooltip())
  }

  @HostListener('mouseout')
  mouseOut() {
    this.view.clear()
  }

}
