import { Directive, effect, ElementRef, inject, input, Renderer2 } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { NavigationEnd, Router } from '@angular/router';
import { Subject, Subscription, takeUntil } from 'rxjs';

@Directive({
  selector: '[appMyRouterLinkActive]',
  standalone: true
})
export class MyRouterLinkActiveDirective {
  appMyRouterLinkActive = input.required<string>()
  el = inject(ElementRef<HTMLElement>)
  renderer = inject(Renderer2)
  router = inject(Router)

  constructor() {
    this.router.events
      .pipe(takeUntilDestroyed())
      .subscribe(ev => {
      if (ev instanceof NavigationEnd) {
        const routerlinkUrl = this.el.nativeElement.getAttribute('routerLink')
        if (ev.url.includes(routerlinkUrl)) {
          this.renderer.addClass(this.el.nativeElement, this.appMyRouterLinkActive())
        } else {
          this.renderer.removeClass(this.el.nativeElement, this.appMyRouterLinkActive())
        }
      }
    })
  }

}
