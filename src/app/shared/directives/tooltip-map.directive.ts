import { Directive, HostListener, inject, input, ViewContainerRef } from '@angular/core';
import { LeafletComponent } from '../leaflet.component';
import { TooltipContentComponent } from './tooltip-content.component';

@Directive({
  selector: '[appTooltipMap]',
  standalone: true
})
export class TooltipMapDirective {
  appTooltipMap = input.required()
  view = inject(ViewContainerRef)

  @HostListener('mouseover')
  mouseOver() {
    const compo = this.view.createComponent(LeafletComponent)
    compo.setInput('coords', this.appTooltipMap())
    compo.instance.mouseLeave
      .subscribe(() => {
        this.view.clear()
      })
  }

  @HostListener('mouseout')
  mouseOut() {
   // this.view.clear()
  }

}
