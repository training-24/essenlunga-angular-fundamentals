import { Directive, ElementRef, HostBinding, HostListener, inject, input } from '@angular/core';

@Directive({
  selector: '[appUrl]',
  standalone: true
})
export class UrlDirective {
  appUrl = input.required<string>()
  @HostBinding() class = 'cursor-pointer'

  @HostListener('click')
  clickMe() {
    window.open(this.appUrl())
  }

}
