import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appPadding]',
  standalone: true
})
export class PaddingDirective {
  @Input() appPadding: 'sm' | 'xl' = 'sm';

  @HostBinding('style.padding') get pad() {
    return this.appPadding === 'sm' ? '20px' : '50px'
  }
  /*
  ngOnInit() {
    console.log(this.appPadding)
  }
  */
}


// document.getElementById('...').style.padding = '10px'
