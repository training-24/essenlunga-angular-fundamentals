import { Directive, effect, inject, input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIfRoleIs]',
  standalone: true
})
export class IfRoleIsDirective {
  minRole = input.required<string>({ alias: 'appIfRoleIs' })
  authSrv = inject(AuthService)
  tpl = inject(TemplateRef<unknown>)
  view = inject(ViewContainerRef)

  constructor() {
    effect(() => {
      this.view.clear()
      if (this.authSrv.role() === this.minRole()) {
        this.view.createEmbeddedView(this.tpl)
      }
    });
  }

}
