import { Component, input } from '@angular/core';

@Component({
  selector: 'app-tooltip-content',
  standalone: true,
  imports: [],
  template: `
    <p class="bg-white">
      {{ title() }}
    </p>
  `,
  styles: ``
})
export class TooltipContentComponent {
  title = input()
}
