import { Directive, effect, HostBinding, inject, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIfLogged]',
  standalone: true
})
export class IfLoggedDirective {
  authSrv = inject(AuthService)
  tpl = inject(TemplateRef<unknown>)
  view = inject(ViewContainerRef)

  constructor() {
    effect(() => {
      this.view.clear()
      if (this.authSrv.isLogged()) {
        this.view.createEmbeddedView(this.tpl)
      }
    });
  }

}
