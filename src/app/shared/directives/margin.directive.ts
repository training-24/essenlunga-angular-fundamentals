import {
  Directive,
  effect,
  ElementRef,
  HostBinding,
  inject,
  input,
  Input,
  signal,
  ViewContainerRef
} from '@angular/core';
import { CardComponent } from '../card.component';

@Directive({
  selector: '[appMargin]',
  standalone: true,
  host: {
    // '[class.layout-root-menuitem]': 'root()'
  }
})
export class MarginDirective {
  el = inject(ElementRef<HTMLElement>)
  // @Input() set appMargin(val: 'sm' | 'xl') {
  //   const currentCls = this.el.nativeElement.className;
  //   this.el.nativeElement.className = `${currentCls} m-12`
  // }

  appMargin = input.required<'sm' | 'xl'>()

  view = inject(ViewContainerRef)

  constructor() {
    effect(() => {
      const currentCls = this.el.nativeElement.className;
      this.el.nativeElement.className = `${this.el.nativeElement.className} ${this.appMargin() === 'xl' ? 'm-24' : 'm-6'}`
    });
  }

}


