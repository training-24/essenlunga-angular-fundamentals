import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'app-list-item',
  standalone: true,
  imports: [],
  template: `
   <li>xyz</li>
  `,
  styles: ``
})
export class ListItemComponent {
  @HostBinding() class = 'pippo'
}
