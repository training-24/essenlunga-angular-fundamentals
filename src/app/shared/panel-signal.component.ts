import { ChangeDetectionStrategy, Component, computed, effect, input, model } from '@angular/core';

@Component({
  selector: 'app-panel-signal',
  standalone: true,
  imports: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <p >
      panel-signal works! {{counter()}}
      
      double {{  double()}}
    </p>
  `,
  styles: ``
})
export class PanelSignalComponent {
  counter = model(123)

  constructor() {
    effect(() => {
      console.log('counter changed', this.counter())
    });
  }
  double = computed(() => {
    return this.counter() * 2
  })

}
