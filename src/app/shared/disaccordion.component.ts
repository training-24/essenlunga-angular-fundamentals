import { Component, contentChildren, effect, input, untracked, viewChild, viewChildren } from '@angular/core';
import { DisaccordionItemComponent } from './disaccordion-item.component';

@Component({
  selector: 'app-disaccordion',
  standalone: true,
  imports: [],
  template: `
   
    <div class="border-2 border-sky-400 p-3">
      <ng-content></ng-content>
    </div>
  `,
  styles: ``
})
export class DisaccordionComponent {
  isFirstOpen = input(true)
  panels = contentChildren(DisaccordionItemComponent)

  constructor() {
    effect(() => {
      if(untracked(this.isFirstOpen)) {
        this.panels()[0].isOpen.set(true)
      }

      this.panels().forEach(panel => {
        panel.toggle.subscribe(res => {
          this.closeAll()
          panel.isOpen.update(prev => true)
        })
      })
    }, { allowSignalWrites: true });

    effect(() => {
    })
  }

  closeAll() {
    this.panels().forEach(panel => panel.isOpen.set(false))
  }
}
