export interface Todo {
  id: number;
  title: string;
  completed: boolean;
  // role: Role
}

export type Role = 'admin' | 'guest'

