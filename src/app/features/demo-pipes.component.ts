import { DatePipe, DecimalPipe, JsonPipe, NgClass, UpperCasePipe } from '@angular/common';
import { Component, Input } from '@angular/core';
import { MemoryPipe } from '../shared/memory.pipe';
import { TitleComponent } from '../shared/title.component';

@Component({
  selector: 'app-demo-pipes',
  standalone: true,
  imports: [
    JsonPipe,
    UpperCasePipe,
    DatePipe,
    NgClass,
    DecimalPipe,
    TitleComponent,
    MemoryPipe
  ],
  template: `
     <app-title>Pipes Demo {{title}}</app-title>
     <div>Date: {{today | date: 'dd/MM/yyyy @ mm:ss' }}</div>
     <div>Number: {{value | number: '1.2-4'}}</div>
     <div>Text: {{text | uppercase}} - {{text}}</div>
     <pre> {{ obj | json  }} </pre>
     
     <div>{{1000 | memory: 'mb' : 'megabytes' | json}}</div>
     <div>{{1000 | memory: 'mb' : 'megabytes'}}</div>
     
     
  `,
  styles: ``
})
export default class DemoPipesComponent {
  today =  Date.now()
  value = 1.129878127012
  text = 'Esselunga'

  @Input() title: string | undefined

  obj = {
    id: 1, name: 'Fabio'
  }
}

export class Pippo {}

export const PI = 3.14
