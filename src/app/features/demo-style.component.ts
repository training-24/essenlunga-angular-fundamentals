import { NgClass } from '@angular/common';
import { Component } from '@angular/core';
import { TitleComponent } from '../shared/title.component';

@Component({
  selector: 'app-demo-styles',
  standalone: true,
  imports: [
    NgClass,
    TitleComponent
  ],
  template: `
    <app-title>Demo Styling</app-title>
    <div
      class="alert "
      [ngClass]="{
        'alert-error text-3xl': alert.type === 'danger',
        'alert-success': alert.type === 'success',
        'alert-info': alert.type === 'info',
      }"
      [style.color]="alert.textColor"
      [style.padding.px]="alert.type === 'danger' ? 30 : 0"
    >
      {{ alert.msg }}
    </div>
    <div class="flex flex-col sm:flex-row gap-3">
      <button (click)="changeColor('danger')">danger</button>
      <button (click)="changeColor('success')">success</button>
      <button (click)="changeColor('info')">info</button>
    </div>
  `
})
export default class DemoStyleComponent {
  alert: Alert = {
    type: 'info',
    msg: 'hello alert!',
    textColor: '#fff000',
    padding: 50
  }
  changeColor(theme: AlertVariant) {
    this.alert = {
      ...this.alert,
      type: theme
    }
  }
}


type Alert = {
  type: AlertVariant;
  msg: string;
  padding: number;
  textColor: string;
}
type AlertVariant = 'info' | 'danger' | 'success'


