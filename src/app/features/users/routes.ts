import { Routes } from '@angular/router';

export const USERS_ROUTES: Routes = [
  {
    path: 'users',
    loadComponent: () => import('./users.component'),
    children: [
      { path: 'anagrafica', loadComponent: () => import('./pages/anagrafica.component')},
      { path: 'gruppi', loadComponent: () => import('./pages/gruppi.component')},
      { path: 'ruoli', loadComponent: () => import('./pages/ruoli.component')},
      { path: '', redirectTo: 'anagrafica', pathMatch: 'full'},
    ]
  },
]
