import { Component } from '@angular/core';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-users',
  standalone: true,
  imports: [
    RouterLink,
    RouterOutlet,
    RouterLinkActive
  ],
  template: `
    
    <button class="btn" routerLinkActive="bg-red-500" routerLink="anagrafica">anagrafica</button>
    <button class="btn" routerLinkActive="bg-red-500" routerLink="ruoli">roles</button>
    <button class="btn" routerLinkActive="bg-red-500" routerLink="/users/gruppi">gruppi</button>
    
    <hr>
    
    <router-outlet />
  `,
  styles: ``
})
export default class UsersComponent {

}
