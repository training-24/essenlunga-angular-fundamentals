import { NgIf } from '@angular/common';
import { Component, computed, effect, signal } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CardComponent } from '../shared/card.component';
import { PanelSignalComponent } from '../shared/panel-signal.component';
import { TitleComponent } from '../shared/title.component';

@Component({
  selector: 'app-demo-signal-1',
  standalone: true,
  imports: [
    NgIf,
    CardComponent,
    TitleComponent,
    FormsModule,
    PanelSignalComponent
  ],
  template: `
    <app-title>Signal</app-title>
    
    <div class="centered-page sm">
      <button class="btn" (click)="dec()">-</button>
      <span
        class="text-2xl"
        [style.color]="isZeroColor()"
      >
        {{counter()}}
      </span>
      <button class="btn" (click)="inc()">+</button>

      <button class="btn" (click)="reset()">reset</button>
      <button class="btn" (click)="randomMsg()">random msg {{msg()}}</button>
    </div>
    
    @if(isZero()) {
      <div>maggiore zero</div>
    }
    
    <input type="text" ngModel>
    <app-panel-signal [counter]="counter()" />
  `,
  styles: ``
})
export default  class DemoSignal1Component {
  counter = signal(0)
  msg = signal('')
  isZero = computed(() => this.counter() > 0)
  isZeroColor = computed(() => this.isZero() ? 'green' : 'red')

  randomMsg() {
    this.msg.set('msg' + Math.random())
  }

  dec() {
    this.counter.update(current => current - 1)
  }

  inc() {
    this.counter.set(this.counter() + 1)
  }

  reset() {
    this.counter.set(0)
  }
}
