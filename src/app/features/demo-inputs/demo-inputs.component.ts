import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, ViewChild } from '@angular/core';
import { CardComponent } from '../../shared/card.component';
import { TitleComponent } from '../../shared/title.component';

@Component({
  selector: 'app-demo-inputs',
  standalone: true,
  imports: [
    CardComponent,
    TitleComponent
  ],
  template: `
    <app-title>Demo Styling</app-title>
   
    <input
      type="text" class="input input-bordered w-96 mb-8"
      (keydown.enter)="changeHandler($event)"
      #inputReference
      placeholder="Write URL and press Enter"
    >
    
    <!--<app-card title="pippo" #card>lorem...</app-card>-->
    <app-card title="pluto1">lorem...</app-card>
    <app-card title="pluto2">lorem...</app-card>
  `,
  styles: ``
})
export default class DemoInputsComponent implements AfterViewInit {
  @ViewChild('inputReference') inputRef: ElementRef<HTMLInputElement> | undefined;
  @ViewChild('card') card: CardComponent | undefined
  @ViewChild(CardComponent) card2: CardComponent | undefined

  changeHandler(event: Event) {
    const input = event.target as HTMLInputElement;
    console.log('hello', input.value)
    window.open(`https://it.wikipedia.org/wiki/${input.value}`)
  }

  ngAfterViewInit(): void {
    this.inputRef?.nativeElement.focus()

      if(this.card2)
        console.log(this.card2)

  }

}

