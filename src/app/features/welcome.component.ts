import { Component } from '@angular/core';
import { TitleComponent } from '../shared/title.component';
import { WeatherSignalComponent } from '../shared/weather-signal.component';
import { WeatherComponent } from '../shared/weather.component';

@Component({
  selector: 'app-welcome',
  standalone: true,
  imports: [
    WeatherComponent,
    TitleComponent,
    WeatherComponent,
    WeatherSignalComponent
  ],
  template: `
    
    <div class="flex gap-3">
      <button class="btn" (click)="currentCity = 'trieste'">Trieste</button>
      <button class="btn" (click)="currentCity = 'Milano'">Milano</button>
      <button class="btn" (click)="currentCity = 'Palermo'">Palermo</button>
    </div>

    <app-title>Weather Demo</app-title>
    <app-weather [city]="currentCity" />
    
    <app-title>Weather  with SignalDemo</app-title>
    <app-weather-signal [city]="currentCity" ></app-weather-signal>
    
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur doloremque error est fugiat iste laudantium nemo nesciunt nulla odit reiciendis. A, cumque, deleniti. Consectetur eveniet, maxime quisquam quo sint tempore.
  `,
  styles: ``
})
export default class WelcomeComponent {
  currentCity = 'Rome'
}
