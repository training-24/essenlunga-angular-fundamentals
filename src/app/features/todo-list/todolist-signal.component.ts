import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, OnInit, signal } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Todo } from '../../model/todo';
import { HttpErrorComponent } from '../../shared/http-error.component';
import { TitleComponent } from '../../shared/title.component';
import { TodosFormComponent } from './components/todos-form.component';
import { TodosListComponent } from './components/todos-list.component';
import { TodosSummaryComponent } from './components/todos-summary.component';
import { TodosService } from './services/todos.service';

@Component({
  selector: 'app-demo-signal2',
  standalone: true,
  imports: [
    JsonPipe,
    TitleComponent,
    HttpErrorComponent,
    FormsModule,
    TodosSummaryComponent,
    TodosFormComponent,
    TodosListComponent
  ],
  template: `
    <app-title>Demo TODO list</app-title>
    <pre>NOTE: Run server with <code>npm run server</code></pre>
    <hr>

    <app-http-error [error]="todosSrv.error()" />
    <app-todos-summary [todos]="todosSrv.todos()"/>
    <app-todos-form (save)="todosSrv.addTodo($event)" />
    <app-todos-list [todos]="todosSrv.todos()" (delete)="todosSrv.deleteTodo($event)" />
  `,
  providers: [TodosService],

})
export default class TodolistSignalComponent implements OnInit {
  todosSrv = inject(TodosService)

  ngOnInit() {
    this.todosSrv.load();
  }
}
