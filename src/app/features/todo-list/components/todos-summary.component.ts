import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { Todo } from '../../../model/todo';

@Component({
  selector: 'app-todos-summary',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  template: `
    <div>Total Todo {{todos().length}}</div>
    <div>Todo Completed: {{todosCompleted()}} </div>
    <div>Todo Incompleted: {{todosIncompleted()}} </div>
  `,
})
export class TodosSummaryComponent {
  todos = input.required<Todo[]>()
  todosCompleted = computed(() => this.todos().filter(t => t.completed).length)
  todosIncompleted = computed(() => this.todos().filter(t => !t.completed).length)
}
