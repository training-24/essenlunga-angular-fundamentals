import { Component, Input, input, output } from '@angular/core';
import { Todo } from '../../../model/todo';

@Component({
  selector: 'app-todos-list-items',
  standalone: true,
  imports: [],
  template: `
    <li>
      <input type="checkbox" [checked]="todo().completed">
      {{todo().title}}
      <button 
        class="btn btn-error" 
        (click)="deleteItem.emit(todo())"
      >Del</button>
      <button (click)="isOpen = !isOpen">expand</button>
    </li>

    @if(isOpen) {
      <div>
        contenuto
      </div>
    }
  `,
  styles: ``
})
export class TodosListItemsComponent {
  todo = input.required<Todo>()
  deleteItem = output<Todo>()
  isOpen = false

}
