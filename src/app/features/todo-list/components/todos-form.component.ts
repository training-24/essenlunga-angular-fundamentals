import { Component, EventEmitter, output, Output } from '@angular/core';

@Component({
  selector: 'app-todos-form',
  standalone: true,
  imports: [],
  template: `
    <input
      type="text" (keydown.enter)="save.emit(inputRef.value)"
      #inputRef
      class="input input-bordered"
      placeholder="add new todo"
    >

  `,
  styles: ``
})
export class TodosFormComponent {
  save = output<string>()
}
