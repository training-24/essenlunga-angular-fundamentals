import { Component, input, output } from '@angular/core';
import { Todo } from '../../../model/todo';
import { TodosListItemsComponent } from './todos-list-items.component';

@Component({
  selector: 'app-todos-list',
  standalone: true,
  imports: [
    TodosListItemsComponent
  ],
  template: `
    @for(todo of todos(); track todo.id) {
      <app-todos-list-items
        [todo]="todo" 
        (deleteItem)="delete.emit($event)"
      />
    }
  `,
})
export class TodosListComponent {
  todos = input.required<Todo[]>()
  delete = output<Todo>()
}
