import { HttpClient } from '@angular/common/http';
import { inject, Injectable, signal } from '@angular/core';
import { Todo } from '../../../model/todo';

@Injectable()
export class TodosService {
  http = inject(HttpClient)
  // todos: Todo[] = [];
  todos = signal<Todo[]>([])
  error = signal(false)


  load() {
    this.http.get<Todo[]>('http://localhost:3000/todos')
      .subscribe({
        next: (res) => {
          this.todos.set(res);
        },
        error: () => {
          this.error.set(true)
        },
      })
  }

  deleteTodo(todo: Todo) {
    this.error.set(false)
    this.http.delete(`http://localhost:3000/todos/${todo.id}`)
      .subscribe({
        next: () => {
          this.todos.update(todos => {
            return todos.filter(t => t.id !== todo.id)
          })
        },
        error: () => {
          this.error.set(true)
        },
      })
  }

  addTodo(title: string) {
    const todo: Omit<Todo, 'id'> = {
      title,
      completed: false
    }
    this.http.post<Todo>(`http://localhost:3000/todos/`, todo)
      .subscribe((newTodo) => {
        // this.todos = [...this.todos, newTodo ]

        this.todos.update(todos => [...todos, newTodo])
      })
  }

}
