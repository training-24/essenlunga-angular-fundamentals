import { Component, effect, inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../core/auth.service';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [],
  providers: [],
  template: `
    <p>
      login works! 
    </p>

    <button
      class="btn"
      (click)="authService.login('a')">Login
    </button>
  `,
  styles: ``
})
export default class LoginComponent {
  authService = inject(AuthService)
  router = inject(Router)

  constructor() {
    effect(() => {
      if (this.authService.isLogged()) {
        this.router.navigateByUrl('demo-pipes')
      }
    }, { allowSignalWrites: true });
  }
}
