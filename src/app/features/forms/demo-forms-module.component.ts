import { JsonPipe } from '@angular/common';
import { Component, input } from '@angular/core';
import { FormsModule, NgForm, NgModel, ReactiveFormsModule } from '@angular/forms';
import { UrlDirective } from '../../shared/directives/url.directive';
import { AlphaNumericDirective } from '../../shared/forms/alpha-numeric.directive';

@Component({
  selector: 'app-demo-uikit10',
  standalone: true,
  imports: [
    JsonPipe,
    FormsModule,
    UrlDirective,
    AlphaNumericDirective,
  ],
  template: `
    <form #myForm="ngForm" (submit)="save(myForm)" class="flex flex-col gap-3 my-4">
      
      {{inputName.errors | json}}
      <input
        #inputName="ngModel"
        class="input input-bordered" type="text" [ngModel]="formData.name" name="name" 
        required minlength="3"
        alphaNumeric
      >
      <input class="input input-bordered" type="text" [ngModel]="formData.surname" name="surname" required>
      <button type="submit" class="btn" [disabled]="myForm.invalid">save</button>
    </form>
  `,
  styles: ``
})
export default class DemoFormsModuleComponent {
  formData = { name: '', surname: ''};

  constructor() {
    console.clear();
  }
  save(myForm: NgForm) {
    console.log(myForm)
  }

  protected readonly input = input;
}
