import { JsonPipe, NgClass } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, inject, input, viewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { debounceTime, delay, map, mergeAll, mergeMap } from 'rxjs';
const REGEX_ALPHA_NUMERIC = /^\w+$/

@Component({
  selector: 'app-demo-uikit9',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    NgClass,
    JsonPipe
  ],
  template: `
    <!--@if(inputName.errors) {
      @if(inputName.hasError('required')) {
        <div>required field</div>
      }
      @if(inputName.hasError('pattern')) {
        <div>No symbols</div>
      }
      
      @if(inputName.hasError('minlength')) {
        &lt;!&ndash;<div>too short (min {{inputName.errors['minlength']['requiredLength']}})</div>&ndash;&gt;
        <div>too short (min {{inputName.getError('minlength')['requiredLength']}})</div>
      }
    }-->
    <!--
  <pre>value: {{form.value | json}}</pre>
  <pre>dirty: {{form.dirty}}</pre>
  <pre>touched: {{form.touched}}</pre>
  <pre>value: {{form.value}}</pre>
  <pre>valid: {{form.valid}}</pre>
  <pre>valid: {{form.errors | json}}</pre>-->

    <div>{{ form.get('name')?.dirty }}</div>

    @if (form.invalid) {
      <div>form invalid</div>
    }
    <form [formGroup]="form" (submit)="save()">
      <input type="text" formControlName="name" class="input input-bordered "
             [ngClass]="{'input-error': form.get('name')?.dirty && form.get('name')?.invalid}"
      >
      <!---->
      <input type="text" formControlName="surname" class="input input-bordered">
      <button class="btn" type="submit">Save</button>

    </form>

  `,
})
export default class DemoForm0Component {
  fb = inject(FormBuilder)
  http = inject(HttpClient)

  form = this.fb.nonNullable.group({
    name: ['', [Validators.required, Validators.minLength(3), Validators.pattern(REGEX_ALPHA_NUMERIC)]],
    surname: ''
  }, { updateOn: 'change'})

  constructor() {
    this.form.valueChanges
      .pipe(
        debounceTime(1000),
        mergeMap(formData => this.http.post<{ id: string, name: string}>('https://jsonplaceholder.typicode.com/users', formData)),
      )
      .subscribe(res => {
        console.log(res)
      })

  }

/*  inputName = new FormControl('', {
    nonNullable: true,
    validators: [Validators.required, Validators.minLength(3), Validators.pattern(REGEX_ALPHA_NUMERIC)]
  });

  inputSurname = new FormControl('b', { nonNullable: true });*/

  save() {
    console.log(this.form)
   // console.log(this.inputName.value, this.inputSurname.value)
  }

}
