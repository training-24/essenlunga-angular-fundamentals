import { JsonPipe, NgClass } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormBuilder,
  ReactiveFormsModule,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { debounceTime, delay, first, map, mergeMap, of, timer } from 'rxjs';

@Component({
  selector: 'app-demo-form-2',
  standalone: true,
  imports: [ReactiveFormsModule, JsonPipe, NgClass],
  template: `

    <form [formGroup]="form">

      <input type="text"
             formControlName="username"
             class="input input-bordered"
             [ngClass]="{'input-error': form.get('username')?.invalid}"
             placeholder="username"
      >
      @if (form.get('username')?.pending) {
        <span>😑</span>
      }

      <br>
      <div formGroupName="passwords">
        @if (form.get('passwords')?.invalid) {
          <div>passwords don't match</div>
        }
        <input type="text" formControlName="password1" class="input input-bordered"
               placeholder="pass 1"
               [ngClass]="{'input-error': form.get('passwords.password1')?.invalid}"
        >
        <input type="text" formControlName="password2" class="input input-bordered"
               [ngClass]="{'input-error': form.get('passwords.password2')?.invalid}"
               placeholder="pass 2">
      </div>

      <div
        formGroupName="anagrafica"
        class="p-3 mt-3"
        [style.border]="form.get('anagrafica')?.valid ? '1px solid green' : null"
      >
        <input type="text" formControlName="name" class="input input-bordered">
        <input type="text" formControlName="surname" class="input input-bordered">
      </div>

      <button class="btn btn-info" [disabled]="form.invalid || form.pending">Save</button>
    </form>

    <pre>{{ form.value | json }}</pre>
    <pre>Valid: {{ form.valid | json }}</pre>
    <pre>errors: {{ pass2?.errors | json }}</pre>

  `,
  styles: ``
})
export default class DemoForm2Component {
  fb = inject(FormBuilder);
  http = inject(HttpClient)


  form = this.fb.nonNullable.group({
    username: ['', Validators.required, userValidator()],
    passwords:
      this.fb.group({
        password1: ['', [Validators.required, Validators.minLength(4)]],
        password2: ['', [Validators.required, Validators.minLength(4)]],
      }, {
        validators: passwordMatch()
      }
    ),
    anagrafica: this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
    })

  })
  pass1 = this.form.get('passwords.password1');
  pass2 = this.form.get('passwords.password2');

  constructor() {
    const result = {
      username: '',
      passwords: {
        password1: 'xyz',
        password2: 'abc',
      }
    }

    // this.form.setValue(result)
  }
  save() {}
}

export function userValidator(): AsyncValidatorFn {
  const http = inject(HttpClient)
  return (c: AbstractControl) => {
    return timer(1000)
      .pipe(
        first(),
        // FLATTENING OPERATOR
        mergeMap(() => http.get<{response: 'ok' | 'error'}>('http://localhost:3000/validate?user=' + c.value).pipe(delay(2000))),
        map(res => res.response === 'ok' ? null : { userNotAvailable: true })
      )
  }
}

export function passwordMatch(params?: any): ValidatorFn {
  return (g: AbstractControl) => {
    const p1 = g.get('password1')
    const p2 = g.get('password2')

    if (p1?.value === p2?.value) {
      return null;
    }

    p2?.setErrors({ /* ...p2?.errors ,*/ passDoesNotPatch: true })

    return {  passwordMatch: true  };
  }
}
