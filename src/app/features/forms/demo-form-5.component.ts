import { Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-demo-form-5',
  standalone: true,
  imports: [ReactiveFormsModule],
  template: `
    <p>
      demo-form-5 works!
    </p>
  `,
  styles: ``
})
export default class DemoForm5Component {

}
