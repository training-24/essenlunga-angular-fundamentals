import { JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  ValidationErrors, ValidatorFn,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-demo-form-1',
  standalone: true,
  imports: [ReactiveFormsModule, JsonPipe],
  template: `
    <form [formGroup]="form" (submit)="save()">
      <div>
        <input type="checkbox" formControlName="isCompany"> isCompany
      </div>

      <!--<div>{{ form.get('name')?.errors | json}}</div>-->
      <input type="text" placeholder="company name" formControlName="name" class="input input-bordered">

      <div>{{ form.get('pi_cf')?.valid | json}}</div>
      <input 
        type="text" 
        [placeholder]="form.get('isCompany')?.value ? 'your vat' : 'your person ID'" 
        formControlName="pi_cf" class="input input-bordered">
      <button class="btn" [disabled]="form.invalid">Save</button>
      
    </form>
    
    <hr>

    {{ form.value | json }}
  `,
  styles: ``
})
export default class DemoForm1Component {
  form = inject(FormBuilder).nonNullable.group({
    name: ['', [Validators.required, Validators.minLength(2), alphaNumericValidator()]],
    pi_cf: [''],
    isCompany: true,
  })

  constructor() {
    this.form.get('isCompany')?.valueChanges
      .subscribe(isCompany => {
        if(isCompany) {
          this.form.get('pi_cf')?.setValidators([Validators.required, lengthValidator(11)])
        } else {
          this.form.get('pi_cf')?.setValidators([Validators.required, lengthValidator(16)])
        }
        this.form.get('pi_cf')?.updateValueAndValidity()
      })

    this.form.get('isCompany')?.setValue(false)
  }

  save() {
    console.log(this.form.value)
    // console.log(this.form.getRawValue())
  }
}

export function alphaNumericValidator(param?: number) {
  return (c: AbstractControl): ValidationErrors | null => {
    if (c.value.length === 0)
      return { empty: true };

    const isValid = /^[a-zA-Z]+$/.test(c.value);
    return isValid ? null : { alphaNumeric:  true  }
  }
}

export function lengthValidator(totalLength: number): ValidatorFn {
  return (c: AbstractControl): ValidationErrors | null => {
    if (c.value.length === totalLength)
      return null;

    return { totalLength: true }
  }
}
