import { Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-demo-form-index',
  standalone: true,
  imports: [ReactiveFormsModule, RouterOutlet, RouterLink, RouterLinkActive],
  template: `
    <div class="join flex-wrap">
      <button class="btn btn-neutral join-item" routerLink="demo-ng-form" routerLinkActive="btn-info">Demo ngForm</button>
      <button class="btn btn-neutral join-item" routerLink="demo-form0" routerLinkActive="btn-info">Demo 0</button>
      <button class="btn btn-neutral join-item" routerLink="demo-form1" routerLinkActive="btn-info">Demo 1</button>
      <button class="btn btn-neutral join-item" routerLink="demo-form2" routerLinkActive="btn-info">Demo 2</button>
      <button class="btn btn-neutral join-item" routerLink="demo-form3" routerLinkActive="btn-info">Demo 3</button>
      <button class="btn btn-neutral join-item" routerLink="demo-form4" routerLinkActive="btn-info">Demo 4</button>
      <button class="btn btn-neutral join-item" routerLink="demo-form5" routerLinkActive="btn-info">Demo 5</button>
    </div>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: ``
})
export default class DemoFormIndexComponent {

}
