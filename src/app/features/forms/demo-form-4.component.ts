import { JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ColorPickerComponent } from '../../shared/forms/color-picker.component';
import { ColorRatingComponent } from '../../shared/forms/color-rating.component';
import { MyCarFormComponent } from '../../shared/forms/my-car-form.component';
import { MyInputComponent } from '../../shared/forms/my-input.component';
import { MyInput2Component } from '../../shared/forms/my-input2.component';
import { RatingComponent } from '../../shared/forms/rating.component';

@Component({
  selector: 'app-demo-form-4',
  standalone: true,
  imports: [ReactiveFormsModule, ColorPickerComponent, JsonPipe, FormsModule, ColorRatingComponent, RatingComponent, MyInputComponent, MyInput2Component, MyCarFormComponent],
  template: `
    <form [formGroup]="form">
     <!-- <div class="">
        <div>Your name</div>
        <input 
          type="text" class="input input-bordered" 
          formControlName="name"
          >
      </div>
      -->
      ...
      <app-my-input 
        formControlName="name" 
        label="your name!!!"
      />
      <app-my-input2 
        formControlName="surname" label="your surname!!!"
        alphaNumeric
      />
      <app-color-picker formControlName="color" />
      <app-rating formControlName="rate"/>
     
      <app-my-car-form formControlName="car" />
    </form>
    
    <br>
    <br>
    <br>
    
     <pre>value: {{form.value | json}}</pre>
     <pre>dirty: {{form.dirty}}</pre>
     <pre>touched: {{form.touched}}</pre>
     <pre>valid: {{form.valid}}</pre>
  `,
  styles: ``
})
export default class DemoForm4Component {
  fb = inject(FormBuilder)
  form = this.fb.nonNullable.group({
    name: ['', Validators.required],
    surname: ['', [Validators.required, Validators.minLength(5)]],
    color: '',
    rate: 1,
    car: {
      brand: '',
      model: ''
    }
  })

  constructor() {
    setTimeout(() => {
      // this.form.get('color')?.disable()
      this.form.patchValue({
       /* name: 'pluto',
        surname: 'ciaone',
        // color: '#90CAF9',
        rate: 5*/
        car: {
          brand: 'Merz',
          model: 'slk'
        }
      })
    }, 3000)
  }
}
