import { JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { TitleComponent } from '../../shared/title.component';

@Component({
  selector: 'app-demo-form-3',
  standalone: true,
  imports: [ReactiveFormsModule, JsonPipe, TitleComponent],
  template: `
    <app-title>Form Array</app-title>
    
    <form [formGroup]="form">
      <input type="text" formControlName="username" class="input input-bordered" placeholder="username">
      
      <div formArrayName="items">
        @for (item of items.controls; track item) {
          <div [formGroupName]="$index">
            <input type="text" placeholder="name" formControlName="name" class="input input-bordered">
            <input type="text" placeholder="cost" formControlName="cost" class="input input-bordered">
            @if(item.valid && $last) {
              <button type="button" class="btn" (click)="addItem()">add</button>
            }
            <button type="button" class="btn btn-error" (click)="removeItem($index)">remove</button>
          </div>
        }
      </div>
    </form>
    
    
    <!--<button class="btn" (click)="addItem('', 0)">Add Item</button>-->
    
    <pre>{{form.value | json}}</pre>
  `,
  styles: ``
})
export default class DemoForm3Component {
  fb = inject(FormBuilder)

  form = this.fb.group({
    username: '',
    items: this.fb.array([])
  })

  items = this.form.get('items') as FormArray

  constructor() {
    this.addItem()
  }

  addItem(name: string = '', cost: number = 0) {
    this.items.push(
      this.fb.group<Product>({
        name: this.fb.control('', { validators: [Validators.required]}),
        cost: new FormControl(null, { validators: [Validators.required]})
      })
    )
  }

  removeItem(index: number) {
    this.items.removeAt(index)
  }

  save() {
    console.log(this.form.value)
  }


}

type Product = {
  name: FormControl<string | null>;
  cost: FormControl<number | null>;
}
/*interface Product {
  name: FormControl<string | null>;
  cost: FormControl<number | null>;
  // price: number | null;
}*/
