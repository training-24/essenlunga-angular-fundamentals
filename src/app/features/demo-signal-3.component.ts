import { Component, inject } from '@angular/core';
import { FakelogService } from '../core/fakelog.service';
import { LogService } from '../core/log.service';

@Component({
  selector: 'app-demo-signal-3',
  standalone: true,
  imports: [],
  template: `
    <p>
      demo-signal-3 works!
    </p>
    
    <button (click)="login('fabio', '123')">SignIN</button>
  `,
  styles: ``,
  providers: [
    { provide: LogService, useExisting: FakelogService }
  ]
})
export default  class DemoSignal3Component {
  logService = inject(LogService)

  login(u: string, p: string) {

  }
}
