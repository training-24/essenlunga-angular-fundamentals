import { Component } from '@angular/core';
import { AccordionItemComponent } from '../../shared/accordion-item.component';
import { AccordionComponent } from '../../shared/accordion.component';
import { TimeLine, TimelineComponent } from '../../shared/timeline.component';
import { TitleComponent } from '../../shared/title.component';

@Component({
  selector: 'app-demo-uikit2',
  standalone: true,
  imports: [
    TimelineComponent,
    TitleComponent,
    AccordionItemComponent,
    AccordionComponent
  ],
  template: `
    <app-title>Reusable Timeline</app-title>
    <app-timeline [items]="timelineList" isVertical/>

    <app-title>Reusable Accordion Item</app-title>
   <app-accordion-item 
     groupName="example" 
     title="Chapter 1" 
     buttonLabel="visit"
     (buttonClick)="doSomething()"
   >
     <button class="btn" (click)="doSomething()">one</button>
     <button>two</button>
   </app-accordion-item>

    <app-accordion-item groupName="example" title="Chapter 2" buttonLabel="visit">
     Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, dicta expedita explicabo ipsam natus nobis quisquam quo rem repellat repudiandae tenetur ullam velit vitae voluptates voluptatibus? Hic molestiae nisi quas?
    </app-accordion-item>
    
    <hr>

    <app-title>Reusable Accordion</app-title>
    <app-accordion 
      [data]="timelineList"
      buttonLabel="Action"
      (buttonClick)="doSomethingElse($event)"
    />
    
  `,
  styles: ``
})
export default class DemoUikit2Component {
  timelineList: TimeLine[] = [
    { start: '2014', end: 'description' },
    { start: '2015', end: 'lorem...' },
    { start: '2018', end: 'bla bla' },
    { start: '2022', end: 'another' },
    { start: '2023', end: 'hello' },
  ]

  doSomethingElse(value: TimeLine) {
    console.log('do something', value)
  }
  doSomething() {
    console.log('do something')
  }
}
