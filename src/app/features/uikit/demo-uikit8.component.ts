import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterLinkActive } from '@angular/router';
import { MarginDirective } from '../../shared/directives/margin.directive';
import { MyRouterLinkActiveDirective } from '../../shared/directives/my-router-link-active.directive';
import { PaddingDirective } from '../../shared/directives/padding.directive';
import { StopPropagationDirective } from '../../shared/directives/stop-propagation.directive';
import { TooltipMapDirective } from '../../shared/directives/tooltip-map.directive';
import { TooltipDirective } from '../../shared/directives/tooltip.directive';
import { UrlDirective } from '../../shared/directives/url.directive';
import { ListItemComponent } from '../../shared/list-item.component';
import { TitleComponent } from '../../shared/title.component';

@Component({
  selector: 'app-demo-uikit8',
  standalone: true,
  imports: [
    PaddingDirective,
    MarginDirective,
    FormsModule,
    ListItemComponent,
    UrlDirective,
    StopPropagationDirective,
    RouterLinkActive,
    MyRouterLinkActiveDirective,
    TooltipDirective,
    TooltipMapDirective,
    TitleComponent
  ],
  template: `
    <app-title> Directive Examples</app-title>
    <div>
      <button appTooltip="descrizione!">trieste</button>
      <button [appTooltipMap]="[43,13]">trieste</button>
    </div>
    
    <div appMyRouterLinkActive="bg-yellow-600" appMargin="xl" class="btn btn-primary">MARGIN</div>
    <div appPadding="xl">Fabio Biondi</div>
    
    <button appUrl="http://www.google.com">google</button>
    <div appUrl="http://www.facebook.com">facebook</div>

    <div (click)="parent()" class="bg-orange-600 p-4">
      parent
      <div appStopPropagation (click)="child()" class="bg-green-600 p-4">
        child
      </div>
    </div>
  `,
  styles: ``
})
export default class DemoUikit8Component {
  parent() {
    console.log('parent')
  }
  child() {
    console.log('child')
  }
}
