import { Component } from '@angular/core';
import { FxComponent } from '../../shared/fx.component';
import { TitleComponent } from '../../shared/title.component';

@Component({
  selector: 'app-demo-uikit6',
  standalone: true,
  imports: [
    FxComponent,
    TitleComponent
  ],
  template: `
    <app-title>Flexbox component </app-title>
    <app-fx gap="sm" justify="start">
      <div>A</div>
      <div>B</div>
      <div>C</div>
    </app-fx>
    
    <hr>
    <div>
      <app-fx>
        <div>label<br/>su due righe</div> 
        <div>icona</div>
      </app-fx>   
      <div>2</div>   
      <div>3</div>   
    </div>
    
    
  `,
  styles: ``
})
export default class DemoUikit6Component {
  value: 'sm' | 'xl' = 'xl'
}
