import { Component, signal } from '@angular/core';
import { SidePanelComponent } from '../../shared/side-panel.component';
import { TitleComponent } from '../../shared/title.component';

@Component({
  selector: 'app-demo-uikit3',
  standalone: true,
  imports: [
    TitleComponent,
    SidePanelComponent
  ],
  template: `
    <app-title>Demo Side Panel</app-title> 
    
    <div class="flex justify-end">
      <pre>{{isOpen}}</pre>
      
      <button class="btn" (click)="isOpen = true">Open side panel</button>
      <button class="btn" (click)="isOpen = false">Close side panel</button>
    </div>
    
    <app-side-panel 
      title="SIDE PANEL"
      [(opened)]="isOpen"
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, debitis doloremque doloribus eaque modi quod temporibus? At deleniti dolor, dolorum ipsa nam optio porro quas, reiciendis tempora tenetur ut voluptates?
      
      .... bla bla ....
      
      <button class="btn" (click)="doSomething()">BUTTON</button>
      
    </app-side-panel>
  `,
  styles: ``
})
export default class DemoUikit3Component {
  isOpen = false;

  doSomething() {
    console.log('doSomething')
  }
}
