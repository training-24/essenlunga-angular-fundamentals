import { AfterViewInit, Component, effect, ElementRef, viewChild, ViewChild, viewChildren } from '@angular/core';
import { PanelSignalComponent } from '../../shared/panel-signal.component';
import { TitleComponent } from '../../shared/title.component';

@Component({
  selector: 'app-demo-uikit4',
  standalone: true,
  imports: [
    PanelSignalComponent,
    TitleComponent
  ],
  template: `
    <app-title>View Child vs View Children</app-title>
    <input type="text" #inputRef>

    <app-panel-signal />
    <app-panel-signal />
    <app-panel-signal />
    
  `,
  styles: ``
})
export default class DemoUikit4Component {
  input = viewChild<ElementRef<HTMLInputElement>>('inputRef')
  panel = viewChild<PanelSignalComponent>(PanelSignalComponent)
  panels = viewChildren(PanelSignalComponent)

  constructor() {
    effect(() => {
      console.log(this.input()?.nativeElement)
    });
    effect(() => {
      console.log(this.panel()?.counter())
      // this.panel()!.counter.set(222)
    }, { allowSignalWrites: true });

    effect(() => {
      console.log('panels', this.panels())
      this.panels().forEach((panel, index) => {
        panel.counter.set(index)
      })
    }, { allowSignalWrites: true });
  }
  /*@ViewChild('inputRef')
  inputRef!: ElementRef<HTMLInputElement> ;

  ngAfterViewInit() {
      this.inputRef.nativeElement.value = 'ciao'
      this.inputRef.nativeElement.focus()
  }*/
}
