import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CardComponent } from '../../shared/card.component';
import { TitleComponent } from '../../shared/title.component';

@Component({
  selector: 'app-demo-uikit1',
  standalone: true,
  imports: [
    CardComponent,
    FormsModule,
    TitleComponent
  ],
  template: `
    <app-title>Demo Reusable Card</app-title>
    
    <h1 class="text-3xl">Card</h1>

    <app-card
      [title]="'Card Title' + counter"
      [(isOpen)]="isCardOpened"
      buttonLabel="Visit Site"
      buttonAlignment="start"
      src="https://img.daisyui.com/images/stock/photo-1606107557195-0e29a4b5b4aa.jpg"
      (buttonClick)="doSomething()"
      showFooter
      cols="3"
      [enableSomething]="true"
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab cupiditate, eligendi eum ipsam officia voluptate. Ab cumque exercitationem modi molestiae nemo quae veritatis voluptatum? Aliquid nesciunt nobis repellendus sapiente voluptatem.
    </app-card>

    <button
      (click)="isCardOpened = !isCardOpened">toggle</button>
    
    <button
      (click)="counter = counter + 1">click me</button>
  `,
  styles: ``
})
export default class DemoUikit1Component {
  value = 'ciao'
  counter = 0;
  isCardOpened = false;

  doSomething() {
    console.log('doSomething');
  }
}

