import { Component, effect, signal, viewChildren } from '@angular/core';
import { DisaccordionItemComponent } from '../../shared/disaccordion-item.component';
import { DisaccordionComponent } from '../../shared/disaccordion.component';
import { LeafletComponent, MapCoords } from '../../shared/leaflet.component';
import { TitleComponent } from '../../shared/title.component';

@Component({
  selector: 'app-demo-uikit5',
  standalone: true,
  imports: [
    TitleComponent,
    DisaccordionItemComponent,
    DisaccordionComponent,
    LeafletComponent
  ],
  template: `
    <app-title>Leaflet</app-title>

    <button (click)="myCoords.set([44, 12])">location 1</button>
    <button (click)="myCoords.set([44, 11])">location 2</button>
    <button (click)="myCoords.set([43, 11])">location 3</button>
    
    <app-leaflet [coords]="myCoords()" />
    <app-leaflet [coords]="[43, 13]" />
    
    
    <app-title>Disaccordion</app-title>
    
    <app-disaccordion [isFirstOpen]="true">
      <app-disaccordion-item title="one">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam blanditiis corporis cum cumque fugiat iure magni molestiae officia officiis omnis quaerat quisquam, quos rem saepe unde vel, veniam voluptas.
      </app-disaccordion-item>
      <app-disaccordion-item title="two">
        <input type="text">
        <input type="text">
      </app-disaccordion-item>
      <app-disaccordion-item title="three" >
        bla bla
      </app-disaccordion-item>
    </app-disaccordion>


    <app-disaccordion-item title="one">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam blanditiis corporis cum cumque fugiat iure magni molestiae officia officiis omnis quaerat quisquam, quos rem saepe unde vel, veniam voluptas.
    </app-disaccordion-item>
    
    
  `,
  styles: ``
})
export default class DemoUikit5Component {
  myCoords = signal<MapCoords>([51.505, -0.09])
  // list = this.store.selectSignal(selectSideBarStatus)
  // filterList = computed(() => this.list().filter.....)
}
