import { NgComponentOutlet, NgIf } from '@angular/common';
import { Component, effect, inject, signal, TemplateRef, viewChild, ViewContainerRef } from '@angular/core';
import { CompoLoaderComponent } from '../../shared/compo-loader/compo-loader.component';
import { TitleComponent } from '../../shared/title.component';
import { ToComponentPipe } from '../../shared/to-component.pipe';

@Component({
  selector: 'app-demo-uikit7',
  standalone: true,
  imports: [
    NgIf,
    CompoLoaderComponent,
    NgComponentOutlet,
    ToComponentPipe,
    TitleComponent
  ],
  template: `
    <app-title>Dynamic component loaders</app-title>
    <ng-template #tpl>
      TPL
    </ng-template>
    
    

    @for(compo of components(); track $index) {
      <app-compo-loader [component]="compo" />
    }

    @for(compo of components2(); track $index) {
      <ng-template *ngComponentOutlet="compo.type | toComponent; inputs: compo.inputs" />
    }
    
  `,
  styles: ``
})
export default class DemoUikit7Component {
  view = inject(ViewContainerRef);
  tpl = viewChild<TemplateRef<unknown>>('tpl')

  components2 = signal([
    {
      type: 'card',
      inputs: {
        title: 'sono una card!!!!',
        variant: 'danger'
      }
    },
    {
      type: 'weather',
      inputs: {
        city: 'Palermo'
      }
    }
  ])

  components = signal([
    {
      type: 'card',
      inputs: {
        title: 'sono una card!!!!',
        variant: 'danger'
      }
    },
    {
      type: 'weather',
      inputs: {
        city: 'Palermo'
      }
    }
  ])


  constructor() {
    effect(() => {
      this.view.createEmbeddedView(this.tpl()!)
      this.view.createEmbeddedView(this.tpl()!)
      this.view.createEmbeddedView(this.tpl()!)

    });
  }
}
