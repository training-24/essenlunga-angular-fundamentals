import { Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { MyRouterLinkActiveDirective } from '../../shared/directives/my-router-link-active.directive';

@Component({
  selector: 'app-uikit-index',
  standalone: true,
  imports: [ReactiveFormsModule, RouterOutlet, RouterLink, RouterLinkActive, MyRouterLinkActiveDirective],
  template: `
    <div class="join flex-wrap">
      <div class="btn btn-neutral join-item" routerLink="uikit1" routerLinkActive="btn-info">uikit1</div>
      <div class="btn btn-neutral join-item" routerLink="uikit2" routerLinkActive="btn-info">uikit2</div>
      <div class="btn btn-neutral join-item" routerLink="uikit3" routerLinkActive="btn-info">uikit3</div>
      <div class="btn btn-neutral join-item" routerLink="uikit4" routerLinkActive="btn-info">uikit4</div>
      <div class="btn btn-neutral join-item" routerLink="uikit5" routerLinkActive="btn-info">uikit5</div>
      <div class="btn btn-neutral join-item" routerLink="uikit6" routerLinkActive="btn-info">uikit6</div>
      <div class="btn btn-neutral join-item" routerLink="uikit7" routerLinkActive="btn-info">uikit7</div>
      <div class="btn btn-neutral join-item" routerLink="uikit8" appMyRouterLinkActive="bg-black">uikit8</div>
    </div>
    <router-outlet></router-outlet>
  `,
  styles: ``
})
export default class UikitIndexComponent {

}
