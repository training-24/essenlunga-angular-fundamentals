import { AsyncPipe, JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, effect, inject, input, Input, signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { mergeMap } from 'rxjs';
import { Todo } from '../model/todo';

@Component({
  selector: 'app-todo-details',
  standalone: true,
  imports: [
    RouterLink,
    JsonPipe,
    AsyncPipe
  ],
  template: `
    <h1>Todo Details {{ todoName() }}</h1>

    {{ todo()?.title }}

    <hr>

    <button class="btn" routerLink="/crud/17">17</button>
    <button class="btn" routerLink="../18">18</button>
    <button class="btn" routerLink="../19">19</button>

    {{ (todo$()?.title) }}
  `,
  styles: ``
})
export default class TodoDetailsComponent {
  http = inject(HttpClient)
  todo = signal<Todo | null>(null) ;
  todoId = input<string | null>(null)
  todoName = computed(() => 'ID: ' + this.todoId())
  activateroute = inject(ActivatedRoute);

  todo$ = toSignal(
    inject(ActivatedRoute).params
      .pipe(
        mergeMap(params => this.http.get<Todo>(`http://localhost:3000/todos/${params['todoId']}`))
      )

  )

  router = inject(Router)

  constructor() {

    effect(() => {
      this.http.get<Todo>(`http://localhost:3000/todos/${this.todoId()}`)
        .subscribe(res => {
          this.todo.set(res)
        })
    });
  }

  /*@Input() set todoId(val: string) {
    this.http.get<Todo>(`http://localhost:3000/todos/${val}`)
      .subscribe(res => {
        this.todo.set(res)
      })
  }*/


  /*
  @Input() set todoId(val: string) {
    this.http.get<Todo>(`http://localhost:3000/todos/${val}`)
      .subscribe(res => {
        this.todo.set(res)
      })
  }

*/

  /**/
  /*
  // reactive
  ngOnInit() {
    this.activateRoute.params
      .pipe(
        mergeMap(params => this.http.get<Todo>(`http://localhost:3000/todos/${params['todoId']}`))
      )
      .subscribe(todo => {
        this.todo.set(todo)
      })
  }
*/

  /*
  // imperative
  ngOnInit() {
    console.log('init')
    this.id = this.activateRoute.snapshot.params['todoId'];
    this.loadTodo(this.id!)
  }

  nextTodo() {
    this.id = +this.id! + 1
    this.router.navigateByUrl(`/crud/${this.id}`)
    this.loadTodo(this.id)
  }

  loadTodo(id: number) {
    const todo = this.http.get<Todo>(`http://localhost:3000/todos/${id}`)
    todo.subscribe(res => {
      this.todo.set(res)
    })
  }

   */
}
