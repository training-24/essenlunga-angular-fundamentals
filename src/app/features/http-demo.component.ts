import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject, OnInit } from '@angular/core';
import { Todo } from '../model/todo';
import { TitleComponent } from '../shared/title.component';

@Component({
  selector: 'app-http-demo',
  standalone: true,
  imports: [
    JsonPipe,
    TitleComponent
  ],
  template: `
    <app-title>Demo TODO list</app-title>
    <pre>NOTE: Run server with <code>npm run server</code></pre>
    
    <br>
    
    <input
      type="text" (keydown.enter)="addTodo(inputRef.value)" #inputRef
      class="input input-bordered"
      placeholder="add new todo"
    >
    
    @for(todo of todos; track todo.id) {
      <li>
        <input type="checkbox" [checked]="todo.completed">
        {{todo.title}}
        <button class="btn btn-error" (click)="deleteTodo(todo)">Del</button>
      </li>
    }
    
  `,
  styles: ``
})
export default class HttpDemoComponent implements OnInit {
  http = inject(HttpClient)
  todos: Partial<Todo>[] = [];

  ngOnInit() {
    this.load();
  }

  load() {
    this.http.get<Todo[]>('http://localhost:3000/todos')
      .pipe(
      )
      .subscribe(res => {
        this.todos = res;
      })
  }

  deleteTodo(todo: Partial<Todo>) {
    this.http.delete(`http://localhost:3000/todos/${todo.id}`)
      .subscribe(() => {
        this.todos = this.todos.filter(t => t.id !== todo.id)
      })
  }

  addTodo(title: string) {
    const todo: Omit<Todo, 'id'> = {
      title,
      completed: false
    }
    this.http.post<Todo>(`http://localhost:3000/todos/`, todo)
      .subscribe((newTodo) => {
        this.todos = [...this.todos, newTodo ]
      })
  }
  function(todo: Partial<Todo>) {
    this.todos = [...this.todos, todo];
  }
}
