import { Component } from '@angular/core';
import { WidgetComponent } from '../shared/widget/widget.component';
import { WidgetService } from '../shared/widget/widget.service';

@Component({
  selector: 'app-widgets-demo',
  standalone: true,
  imports: [
    WidgetComponent
  ],
  template: `
    <p>
      widgets-demo works!
    </p>
    
    <app-widget />
    <app-widget />
    <app-widget />
  `,
  styles: ``,

})
export default class WidgetsDemoComponent {

}
