import { Component, effect, Inject, inject, OnInit, signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { NavigationEnd, NavigationError, Router, RouterOutlet } from '@angular/router';
import { filter, map } from 'rxjs';
import { environment } from '../environments/environment';
import { NavBarComponent } from './core/nav-bar.component';
import { UserService } from './core/user.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, NavBarComponent],
  template: `
    <app-nav-bar />
    
    <div class="mx-3 mt-3">
      <router-outlet />
    </div>
    
  `,
})
export class AppComponent implements OnInit {
  error = signal(false)
  router = inject(Router)
  userSrv = inject(UserService)
  // superValue = inject('mySuperValue')

  constructor() {

  }

  ngOnInit() {
    this.router.events
      .pipe(
        filter(ev => (ev instanceof NavigationError)),
        map(() => true)
      )
      .subscribe({
        next: isError => {
          this.error.set(isError);
        }
      })

    this.router.events
      .subscribe({
        next: ev => {
          this.error.set(false);

          if (ev instanceof NavigationEnd) {
            // console.log('event route', ev.url)
          }

          if (ev instanceof NavigationError) {
            this.error.set(true);
            //  console.log('error', ev.url)
          }
        }
      })

    /*this.router.events
      .subscribe(ev => {
        this.error.set(false);

        if (ev instanceof NavigationEnd) {
          console.log('ciaone', ev.url)
        }

        if (ev instanceof NavigationError) {
          this.error.set(true);
          console.log('ciaone', ev.url)
        }
      })*/
  }

}
