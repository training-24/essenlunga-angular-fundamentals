import { Routes } from '@angular/router';
import { authGuard } from './core/auth.guard';
import { USERS_ROUTES } from './features/users/routes';

export const routes: Routes = [
  {
    path: 'demo-pipes',
    loadComponent: () => import('./features/demo-pipes.component'),
    // canActivate: [authGuard],
    title: 'Demo Pipes',
    data: { title: 'xyz' },

  },

  { path: 'welcome', loadComponent: () => import('./features/welcome.component')},
  { path: 'demo-styles', loadComponent: () => import('./features/demo-style.component') },
  { path: 'demo-inputs', loadComponent: () => import('./features/demo-inputs/demo-inputs.component')},
  { path: 'demo-widgets', loadComponent: () => import('./features/widgets-demo.component')},

  {
    path: 'demo-forms',
    loadComponent: () => import('./features/forms/demo-form-index.component'),
    children: [
      { path: 'demo-ng-form', loadComponent: () => import('./features/forms/demo-forms-module.component')},
      { path: 'demo-form0', loadComponent: () => import('./features/forms/demo-form-0.component')},
      { path: 'demo-form1', loadComponent: () => import('./features/forms/demo-form-1.component')},
      { path: 'demo-form2', loadComponent: () => import('./features/forms/demo-form-2.component')},
      { path: 'demo-form3', loadComponent: () => import('./features/forms/demo-form-3.component')},
      { path: 'demo-form4', loadComponent: () => import('./features/forms/demo-form-4.component')},
      { path: 'demo-form5', loadComponent: () => import('./features/forms/demo-form-5.component')},
      { path: '', redirectTo: 'demo-ng-form', pathMatch: 'full'},
    ]
  },
  {
    path: 'uikit',
    loadComponent: () => import('./features/uikit/uikit-index.component'),
    children: [
      { path: 'uikit1', loadComponent: () => import('./features/uikit/demo-uikit1.component')},
      { path: 'uikit2', loadComponent: () => import('./features/uikit/demo-uikit2.component')},
      { path: 'uikit3', loadComponent: () => import('./features/uikit/demo-uikit3.component')},
      { path: 'uikit4', loadComponent: () => import('./features/uikit/demo-uikit4.component')},
      { path: 'uikit5', loadComponent: () => import('./features/uikit/demo-uikit5.component')},
      { path: 'uikit6', loadComponent: () => import('./features/uikit/demo-uikit6.component')},
      { path: 'uikit7', loadComponent: () => import('./features/uikit/demo-uikit7.component')},
      { path: 'uikit8', loadComponent: () => import('./features/uikit/demo-uikit8.component')},
      { path: 'uikit11', loadComponent: () => import('./features/uikit/demo-uikit11.component')},
      { path: '', redirectTo: 'uikit1', pathMatch: 'full'},
    ]
  },
  { path: 'login', loadComponent: () => import('./features/login.component')},

  { path: 'crud', loadComponent: () => import('./features/http-demo.component')},
  { path: 'crud/:todoId', loadComponent: () => import('./features/todo-details.component')},
  { path: 'signal1', loadComponent: () => import('./features/demo-signal-1.component')},
  { path: 'signal2', loadComponent: () => import('./features/todo-list/todolist-signal.component')},
  { path: 'signal3', loadComponent: () => import('./features/demo-signal-3.component')},
  ...USERS_ROUTES,
  { path: '', redirectTo: 'welcome', pathMatch: 'full'},
]
